using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildiingManager : MonoBehaviour {

    private BuildingTypeListSO buildingTypeList;
    private BuildingTypeSO buildingType;

    private Camera mainCamera;

    private void Start() {
        
        mainCamera = Camera.main;

        buildingTypeList = Resources.Load<BuildingTypeListSO>(typeof(BuildingTypeListSO).Name);
        buildingType = buildingTypeList.list[0];
    }

    private void Update() {

        TEST();

        if (Input.GetMouseButtonDown(0)) {
            
            Instantiate(buildingType.prefab, GetMouseWorldPosition(), Quaternion.identity);
        }
    }

    private Vector3 GetMouseWorldPosition() {

        Vector3 mouseWorldPosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        mouseWorldPosition.z = 0;

        return mouseWorldPosition;
    }

    private void TEST() {
        
        if (Input.GetKeyDown(KeyCode.Q)) {
            print("Change To Wood Harvester!");
            buildingType = buildingTypeList.list[0];
        }
        else if (Input.GetKeyDown(KeyCode.W)) {
            print("Change To Stone Harvester!");
            buildingType = buildingTypeList.list[1];
        }
    }
}